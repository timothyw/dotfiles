#/bin/sh
DOTPATH=~/Backup/dotfiles
rm  ~/.i3blocks.conf ~/.i3/config ~/.xinitrc
rm ~/.config/xfce4/terminal/terminalrc
ln -s $DOTPATH/terminalrc ~/.config/xfce4/terminal/
ln -s $DOTPATH/i3blocks.conf ~/.i3blocks.conf
ln -s $DOTPATH/i3/config ~/.i3/config
sudo rm -r /usr/libexec/i3blocks
sudo ln -s $DOTPATH/i3blocks /usr/libexec/
ln -s $DOTPATH/xinitrc ~/.xinitrc
